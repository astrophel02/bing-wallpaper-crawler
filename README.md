# 超简单的必应Bing壁纸爬虫

#### 介绍

用29行python代码写的简单（简陋）Bing每日壁纸爬虫

#### 安装教程

仅供学习，如果想要测试，请确保计算机已安装python3，然后直接下载main.py文件即可

#### 使用说明

注意，本程序依赖requests库和beautifulsoup4库

#### 注意

这些代码是学python一星期的产物，仅是为了记录学习过程，请谅解

另外，本程序抓取的是Bing每日壁纸的预加载图片，虽然没有水印，但是清晰度只有1920*1080（大概）