import requests
from bs4 import BeautifulSoup
def get_html(url):            #固定的网页源码获取函数

    try:
        r=requests.get(url)
        r.raise_for_status()
        r.encoding=r.apparent_encoding
        return r.text
    except:
        r="fail"
        return r
if __name__=="__main__":
    url=input("请输入url")             #在这里输入bing网址
    html=get_html(url)                #调用上面的函数
    soup=BeautifulSoup(html,'html.parser')        #使用beautifulsoup库解析html源码，利用python内置的html.parser库
    #title=soup.title.text    #（这行没用）
    img=soup.find("div",attrs={"class":"img_cont"})    #分析bing源码，找到预加载图片的标签位置
    #print (img)
    #print(type(img))
    pu = img.get("style")       #style标签后有链接，进一步缩小位置
    u=pu[22:-14]
    #print(pu)
    img_resp = requests.get(u)          #请求链接，获取图片
    img_name = u.split("/")[-1]
    #img_name = "today.jpg"    #(windows系统请替换为这个)
    with open(img_name,"wb") as f:
        f.write(img_resp.content)    #将图片内容写入文件
    print("恭喜，bing今日图片爬取成功！图片储存在项目根目录中")